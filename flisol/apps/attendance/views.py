# -*- coding: UTF-8 -*-
"""
Views for attendance app

Copyright (C) 2014
Under GPL3

Authors:
 - Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
"""
from django.contrib.admin.views.decorators import staff_member_required
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404

from apps.attendance.forms import PersonForm
from apps.attendance.models import Location, Person, Registry
from apps.attendance.utils import generate_qr
from flisol.settings import SITE_DOMAIN


def home(request):
    """
    Show the home page
    """
    # Get list of all locations
    locations = Location.objects.all().order_by('name')
    data = {
        'locations': locations
    }
    return render(request, 'attendance_index.html', data)


def new_registry(request, location_id):
    """
    Register a new person to the location
    """
    # Get and check location to register
    location = get_object_or_404(Location, id=location_id)
    # get the registration form
    registry_form = PersonForm()

    # Validate data send via POST method
    if request.method == 'POST':
        registry_form = PersonForm(request.POST)
        if registry_form.is_valid():
            # Check if person (email is PK) is already registered on DB
            try:
                person = Person.objects.get(email=request.POST.get('email'))
            except Person.DoesNotExist:
                # Register as new person
                person = registry_form.save()
            # Save new registration
            registry = Registry(location=location,
                                person=person)
            registry.save()
            # Save a session variable to use on confirm registration page
            request.session['registry_code'] = registry.registry_code
            return redirect('/registro/confirmar/')

    data = {
        'location': location,
        'registry_form': registry_form,
    }
    return render(request, 'new_registry.html', data)


def confirm_register(request):
    """
    Confirm a registry
    """
    # Verifiy a session variable of registry exist, or raise a 404 error
    try:
        registry = Registry.objects.get(
            registry_code=request.session['registry_code'])
        # Generate a QR code with registry
        qr_url = 'http://{}/registro/verificar/?reg_code={}'.format(
            SITE_DOMAIN, registry.registry_code)
        qr_code = generate_qr(value=qr_url)
        data = {
            'registry': registry,
            'qr_code': qr_code,
        }
        return render(request, 'confirm_register.html', data)
    except Exception as e:
        print(e)
        raise Http404


def verify_registry(request):
    """
    Verify a registry
    """
    # Get registry code send via GET method
    registry_code = request.GET.get('reg_code', '')
    registry_code.replace('/', '')
    registry = None

    # if there's a registry code, verify if registry exists
    if registry_code:
        try:
            registry = Registry.objects.get(registry_code=registry_code)
        except Registry.DoesNotExist:
            registry = 'not-found'

    data = {
        'registry': registry,
    }
    return render(request, 'verify_registry.html', data)


@staff_member_required
def list_locations(request):
    """
    List locations to see attendees registered
    """
    # Get list of all locations
    locations = Location.objects.all().order_by('name')
    data = {
        'locations': locations
    }
    return render(request, 'locations_registers.html', data)


@staff_member_required
def list_attendees(request, location_id):
    """
    List attendees registered on location
    """
    # Get location
    location = get_object_or_404(Location, id=location_id)
    # Get list of all locations
    attendees = Registry.objects.filter(location=location).order_by(
        'person__last_name')
    data = {
        'location': location,
        'attendees': attendees,
    }
    return render(request, 'list_attendees.html', data)