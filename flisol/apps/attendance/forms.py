# -*- coding: UTF-8 -*-
"""
Forms for attendance app

Copyright (C) 2014
Under GPL3

Authors:
 - Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
"""
from django.forms import ModelForm, TextInput
from django.forms.widgets import EmailInput

from apps.attendance.models import Person


class PersonForm(ModelForm):
    class Meta:
        model = Person
        widgets = {
            'name': TextInput(
                attrs={
                    'required': '',
                    'autofocus': '',
                    'placeholder': 'Nombre(s)'
                }
            ),
            'last_name': TextInput(
                attrs={
                    'required': '',
                    'autofocus': '',
                    'placeholder': 'Apellidos'
                }
            ),
            'email': EmailInput(
                attrs={
                    'required': '',
                    'autofocus': '',
                    'placeholder': u'Correo electrónico'
                }
            )

        }