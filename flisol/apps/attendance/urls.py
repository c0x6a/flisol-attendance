# -*- coding: UTF-8 -*-
"""
URLs for attendance app

Copyright (C) 2014
Under GPL3

Authors:
 - Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
"""
from django.conf.urls import patterns, url

from apps.attendance import views


urlpatterns = patterns(
    '',
    url(r'^$', views.home),
    url(r'^nuevo/(\d+)/$', views.new_registry),
    url(r'^confirmar/$', views.confirm_register),
    url(r'^verificar/(\d+)/$', views.verify_registry),
    url(r'^verificar/$', views.verify_registry),
)

