# -*- coding: UTF-8 -*-
"""
Admin for attendance app

Copyright (C) 2014
Under GPL3

Authors:
 - Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
"""
from django.contrib import admin

# Register your models here.
from apps.attendance.models import Location, Person, Registry

admin.site.register(Location)
admin.site.register(Person)
admin.site.register(Registry)
