# -*- coding: UTF-8 -*-
"""
Utils for attendance app

Copyright (C) 2014
Under GPL3

Authors:
 - Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
"""


def generate_qr(value, size="160x160"):
    return "{}?chs={}&cht=qr&chl={}&choe=UTF-8&chld=H|0".format(
        "http://chart.apis.google.com/chart", size, value)
