# -*- coding: UTF-8 -*-
"""
Models for attendance app

Copyright (C) 2014
Under GPL3

Authors:
 - Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
"""
from django.db import models


class Location(models.Model):
    """
    Location model
    """
    name = models.CharField(max_length=40, unique=True, verbose_name=u'Nombre')
    address = models.CharField(max_length=200, verbose_name=u'Dirección')
    district = models.CharField(max_length=80, verbose_name=u'Distrito')
    province = models.CharField(max_length=80, verbose_name=u'Provincia')

    class Meta:
        verbose_name = u'Sede'
        verbose_name_plural = u'Sedes'

    def __unicode__(self):
        return self.name

    def get_registered_qty(self):
        registers = Registry.objects.filter(location__id=self.id)
        return len(registers)


class Person(models.Model):
    """
    Person model
    """
    name = models.CharField(max_length=40, verbose_name=u'Nombre(s)')
    last_name = models.CharField(max_length=40, verbose_name=u'Apellidos')
    email = models.EmailField(verbose_name=u'Correo electrónico')

    class Meta:
        verbose_name = u'Asistente'
        verbose_name_plural = u'Asistentes'

    def __unicode__(self):
        return self.email

    def get_complete_name(self):
        return '{.name} {.last_name}'.format(self)


class Registry(models.Model):
    """
    Registry model
    """
    registry_code = models.CharField(
        max_length=13, verbose_name=u'Código', primary_key=True)
    location = models.ForeignKey(Location, verbose_name=u'Evento')
    person = models.ForeignKey(Person, verbose_name=u'Persona')

    class Meta:
        unique_together = ('location', 'person')
        verbose_name = u'Registro'
        verbose_name_plural = u'Registros'

    def __unicode__(self):
        return self.registry_code

    def save(self):
        """
        Override self method
        """
        # Generate registry code
        self.registry_code = '201404{:0>3}{:0>5}'.format(
            self.location.id,
            self.person.id)

        super(Registry, self).save()

