# -*- coding: UTF-8 -*-
"""
URLs FLISOL Attendance project

Copyright (C) 2014
Under GPL3

Authors:
 - Carlos Joel Delgado Pizarro <cj@carlosjoel.net>
"""
from django.conf.urls import patterns, include, url

from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', 'apps.attendance.views.home'),
    url(r'^sedes/', 'apps.attendance.views.list_locations'),
    url(r'^sede/(?P<location_id>\d+)/',
        'apps.attendance.views.list_attendees'),
    url(r'^registro/', include('apps.attendance.urls')),
    url(r'^admin/', include(admin.site.urls)),
)

